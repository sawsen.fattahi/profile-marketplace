import mongoose from 'mongoose';

const addressSchema = new mongoose.Schema({
  addressStreet: String,
  addressCity: String,
  addressState: String,
  addressZip: Number,
  addressIsCurrent: Boolean,
  addressCreatedDate: {
    type: Date,
    default: Date.now,
  },
  addressLastUpdateDate: {
    type: Date,
  },
  addressStatus: {
    type: Number,
    enum: [1, 2, 3, 4],
    default: 1,
  },
});

const imageSchema = new mongoose.Schema({
  imageUri: String,
  imageCreatedDate: {
    type: Date,
    default: Date.now,
  },
  imageLastUpdateDate: {
    type: Date,
  },
  imageStatus: {
    type: Number,
    enum: [1, 2, 3, 4],
    default: 1,
  },
});
const productSchema = new mongoose.Schema({
  productName: String,
  productPrice: Number,
  productPricePromo: Number,
  productUnit: String,
  productImages: [imageSchema],
  productCreatedDate: {
    type: Date,
    default: Date.now,
  },
  productLastUpdateDate: {
    type: Date,
  },
  productStatus: {
    type: Number,
    enum: [1, 2, 3, 4],
    default: 1,
  },
});

const marketSchema = new mongoose.Schema({
  marketName: String,
  marketAddress: addressSchema,
  marketPhone: Number,
  marketProducts: [productSchema],
  marketImages: [imageSchema],
  marketCreatedDate: {
    type: Date,
    default: Date.now,
  },
  marketLastUpdateDate: {
    type: Date,
  },
  marketStatus: {
    type: Number,
    enum: [1, 2, 3, 4],
    default: 1,
  },
});

const linkSchema = new mongoose.Schema({
  linkDescription: {
    type: String,
    default: '',
  },
  linkUri: {
    type: String,
    default: '',
  },
  linkCreatedDate: {
    type: Date,
    default: Date.now,
  },
  linkLastUpdateDate: {
    type: Date,
  },
  linkStatus: {
    type: Number,
    enum: [1, 2, 3, 4],
    default: 1,
  },
});

const profileSchema = new mongoose.Schema({
  profileUserId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    unique: true,
  },
  profileEmail: {
    type: String,
    ref: 'User',
    unique: true,
  },
  profileName: String,
  profileImage: imageSchema,
  profileSummary: String,
  profileLookingFor: [String],
  profileTags: [String],
  profileMarkets: [marketSchema],
  profileType: {
    type: Number,
    enum: [1, 2],
    default: 1,
  },
  profileAddress: [addressSchema],
  profileLinks: [linkSchema],
  profileLikes: [{ type: mongoose.Schema.Types.ObjectId, default: '' }],
  profileDislikes: [{ type: mongoose.Schema.Types.ObjectId, default: '' }],
  profileStatus: {
    type: Number,
    enum: [1, 2, 3, 4],
    default: 1,
  },
  profileCreatedDate: {
    type: Date,
    default: Date.now,
  },
  profileLastUpdateDate: {
    type: Date,
  },
});

export default mongoose.model('Profile', profileSchema);
