import {
  addOrUpdateProfileController,
  getProfileHandler,
} from '../handlers';
import { authorisation } from '../middelware';

const profileRoutes = (app) => {
  app.route('/profile').post(authorisation, (req, res) => addOrUpdateProfileController(req, res));
  app.route('/profile').get(authorisation, (req, res) => getProfileHandler(req, res));
  app.route('/profiles').get(authorisation, (req, res) => res.json('profiles'));
  app.route('/market').get(authorisation, (req, res) => res.json('market'));
  app.route('/market').post(authorisation, (req, res) => res.json('market'));
  app.route('/market').put(authorisation, (req, res) => res.json('market'));
  app.route('/markets').get(authorisation, (req, res) => res.json('markets'));
  app.route('/address').get(authorisation, (req, res) => res.json('adress'));
  app.route('/address').post(authorisation, (req, res) => res.json('address'));
  app.route('/address').put(authorisation, (req, res) => res.json('address'));
};

export default profileRoutes;
