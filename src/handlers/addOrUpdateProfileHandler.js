import { addOrUpdateProfileService } from '../services';

const addOrUpdateProfileHandler = async (req, res) => {
  const { body } = req;
  const {
    user, lookingFor, tags, type, name, address, links,
  } = body;
  if (!user) {
    res.json({ status: 300, message: 'user is required!' });
  } else {
    const { id, email } = user;
    const newProfile = {
      profileLookingFor: lookingFor,
      profileTags: tags,
      profileType: type,
      profileUserId: id,
      profileEmail: email,
      profileName: name,
      profileAddress: address,
      profileLinks: links,
    };
    const response = await addOrUpdateProfileService(newProfile);
    res.json(response);
  }
};

export default addOrUpdateProfileHandler;
