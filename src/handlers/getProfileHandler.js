import { getProfileService } from '../services';

const getProfileHandler = async (req, res) => {
    const { body } = req;
    const {
      userId,
    } = body;
    if (!userId) {
      res.json({ status: 300, message: 'user ID is required!' });
    } else {
      
      const response = await getProfileService(userId);
      res.json(response);
    }
}

export default getProfileHandler