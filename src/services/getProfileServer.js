import db from '../models';

const getProfileService = async (profileUserId) => {
  try {
    const profile = await db.Profile;
    const response = await profile.findOne({ profileUserId });
    return { profile: response, status: 200 };
  } catch (err) {
    return { message: err, status: 500 };
  }
};

export default getProfileService;
