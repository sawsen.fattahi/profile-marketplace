import db from '../models';

const addProfileService = async (newProfile) => {
  try {
    const profile = await db.Profile;
    const { profileUserId } = newProfile;
    const response = await profile.findOneAndUpdate({ profileUserId }, newProfile, {
      upsert: true,
      new: true,
    });
    return { profile: response, status: 200 };
  } catch (err) {
    return { message: err, status: 500 };
  }
};

export default addProfileService;
