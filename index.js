import express from "express";
import bodyParser from "body-parser";
import log from "npmlog";
import { profileRoutes } from "./src/routes";
import db from "./src/models";

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const PORT = process.env.PORT || 4022;
profileRoutes(app);
app.listen(PORT, () => {
  log.info("Server is running on: ", PORT);
  db.mongoose
    .connect(db.url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    })
    .then(() => {
      log.info("Server is Connected to the database: ", db.url);
    })
    .catch((err) => {
      log.error("Cannot connect to the database!", err);
      process.exit();
    });
});
